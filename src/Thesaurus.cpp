#include "Thesaurus.h"

Thesaurus::Thesaurus() : SortedStringList() {
  equiv = new PointerList<SortedStringList>;
}


Thesaurus::Thesaurus(const Thesaurus& sourceList) {
  head = NULL;
  length = 0;
  addList(&sourceList);
}


Thesaurus::~Thesaurus() {
  delete equiv;
}


PointerList<SortedStringList>* Thesaurus::getEquiv() {
  return equiv;
}


// returns null when nothing found
SortedStringList* Thesaurus::getClass(string mc) {
  if (!contains(mc)) {
    return NULL;
  }
  PointerListElement<SortedStringList>* first = getEquiv()->getFirst();
  PointerListElement<SortedStringList>* curr = first;
  do {
    if (curr->getValue()->contains(mc)) {
      return curr->getValue();
    }
    curr = curr->getNext();
  } while(curr != first);
  return NULL;
}


size_t Thesaurus::printEquiv() {
  PointerListElement<SortedStringList>* first = getEquiv()->getFirst();
  PointerListElement<SortedStringList>* curr = first;
  int i = 1;
  cout << i << " - Créer une nouvelle classe." << endl;
  if (curr != NULL) {
    do {
      i++;
      cout << i << " - Ajouter à [" << curr->getValue()->generateString() << "]" << endl;
      curr = curr->getNext();
    } while(curr != first);
  }
  return i;
}


// TODO add question for equivalence
void Thesaurus::addElement(string elem, bool silent) {
  if (!contains(elem)) {
    SortedStringList::addElement(elem);
    // EQUIVALENCES:
    if (!silent) {
      cout << "---------------------------------------------------" << endl;
      cout << "CLASSES DES EQUIVALENCES pour <<" << elem << ">>" << endl << endl;
      size_t i = printEquiv();
      size_t choice;
      while((choice = handleOptionInput(i)) == 0) {
        cout << "---------------------------------------------------" << endl;
      }
      if (choice == 1) {
        SortedStringList* newList = new SortedStringList();
        newList->addElement(elem);
        getEquiv()->addElement(newList);
        delete newList;
      } else {
        PointerListElement<SortedStringList>* first = getEquiv()->getFirst();
        PointerListElement<SortedStringList>* curr = first;
        for (size_t j = 1; j < choice-1; j++) {
          curr = curr->getNext();
        }
        curr->getValue()->addElement(elem);
      }
    }
  }
}


void Thesaurus::addList(const SortedStringList* newMots) {
  ListElement<string>* first = newMots->getFirst();
  ListElement<string>* curr = first;

  if (first == NULL) {
    return;
  }
  addElement(curr->getValue());
  curr = curr->getNext();
  while(curr != first) {
    addElement(curr->getValue());
    curr = curr->getNext();
  }
}


void Thesaurus::addClasses(PointerList<SortedStringList>* eqClasses) {
  delete equiv;
  equiv = new PointerList<SortedStringList>(*eqClasses);
}


ostream& operator<<(ostream& os, const Thesaurus& perso) {
  ListElement<string>* head = perso.getFirst();
  if (head != NULL) {
    os << head->getValue() << endl;
    ListElement<string>* next = head->getNext();
    while(next != head) {
      os << next->getValue() << endl;
      next = next->getNext();
    }
  }
  return os;
}
