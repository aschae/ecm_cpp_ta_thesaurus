// #include <iostream>
// #include <string>
// #include <fstream>
// #include <algorithm>
// #include "tools.h"

using namespace std;

// const size_t NUM_ABNORMS = 35;
// const string REPLACERS[NUM_ABNORMS][2] = {{"ä", "ae"}, {"Ä", "ae"}, {"à", "a"}, {"À", "a"},
// {"æ", "ae"}, {"Æ", "ae"}, {"ç", "c"}, {"Ç", "c"}, {"é", "e"}, {"É", "e"},
// {"è", "e"}, {"È", "e"}, {"Ê", "e"}, {"ê", "e"}, {"Ë", "e"}, {"ë", "e"},
// {"Î", "i"}, {"î", "i"}, {"Ï", "i"}, {"ï", "i"}, {"ö", "oe"}, {"Ö", "oe"},
// {"Ô", "o"}, {"ô", "o"}, {"Œ", "oe"}, {"œ", "oe"}, {"ß", "ss"}, {"ü", "ue"},
// {"Ü", "ue"}, {"Ù", "u"}, {"ù", "u"}, {"Û", "u"}, {"û", "u"}, {"Ÿ", "y"}, {"ÿ", "y"}};

// #include "List.h"
// #include "List.cpp"
// #include "PointerList.h"
// #include "PointerListElement.h"
// #include "MotCle.h"
// #include "tools.h"

// #include "SortedStringList.h"
// #include "PointerList.h"
// #include "Stage.h"
// #include "Thesaurus.h"
// #include "TestTemplate.cpp"
// #include "List.cpp"

int main(int argc, char const *argv[]) {


/* delete SortedStringList */
  // SortedStringList* list = new SortedStringList();
  // list->addElement("4");
  // list->addElement("2");
  // list->addElement("7");
  // ListElement<string>* first = list->getFirst();
  // ListElement<string>* second = first->getNext();
  // ListElement<string>* third = second->getNext();
  // delete list;
  // delete first;
  // delete second;
  // delete third;
  // delete list;



/* delete PointerList/Element */
  // PointerList<MotCle>* list = new PointerList<MotCle>();
  // MotCle* mc = new MotCle("Bernd", 2);
  // list->addElement(mc);
  // delete mc;
  // mc = new MotCle("Taube", 7);
  // list->addElement(mc);
  // delete mc;
  // mc = new MotCle("Hund", 0);
  // list->addElement(mc);
  // delete mc;
  // delete list;
  // delete mc;
  // // delete list;
  // // delete first;
  // // delete second;
  // // delete third;


/* delete List */
  // List<int>* list = new List<int>();
  // list->addElement(4);
  // list->addElement(2);
  // list->addElement(7);
  // ListElement<int>* first = list->getFirst();
  // ListElement<int>* second = first->getNext();
  // ListElement<int>* third = second->getNext();
  // delete list;
  // // delete list;
  // // delete first;
  // // delete second;
  // // delete third;


/* copy thesaurus */
  // Thesaurus* thes = new Thesaurus();
  // thes->addElement("Kauz");
  // thes->addElement("Ivan");
  // thes->addElement("Juli");
  // thes->addElement("Juli");
  // Thesaurus* neu = new Thesaurus(*thes);
  // cout << thes << " original: " << endl << *thes << endl;
  // cout << neu << " new: " << endl << *neu << endl;
  // thes->getFirst()->value = "bohne";
  // cout << thes << " original: " << endl << *thes << endl;
  // cout << neu << " new: " << endl << *neu << endl;
  // delete thes;
  // delete neu;


/* copy of Stage */
  // SortedStringList* list = new SortedStringList();
  // list->addElement("Kauz");
  // list->addElement("Ivan");
  // list->addElement("Juli");
  // Stage* intern = new Stage("Praktikum bei VW", "Meier", "Rufus", "2013", list);
  // Stage* ne = new Stage(*intern);
  // cout << intern << " original: " << endl << *intern << endl;
  // cout << ne << " new: " << endl << *ne << endl;
  // list->getFirst()->value = "hier hat list was geaendert";
  // intern->motsCles->getFirst()->next->value = "hier hat sich intern selbst geaendert";
  // cout << intern << " original: " << endl << *intern << endl;
  // cout << ne << " new: " << endl << *ne << endl;
  // delete intern;
  // delete ne;
  // delete list;


/* copy of SortedStringList */
  // SortedStringList* list = new SortedStringList();
  // list->addElement("Kauz");
  // list->addElement("Ivan");
  // list->addElement("Juli");
  // SortedStringList* nlist = new SortedStringList(*list);
  // cout << list << " original: " << endl << *list << endl;
  // cout << nlist << " new: " << endl << *nlist << endl;
  // list->getFirst()->value = "krug";
  // list->getFirst()->next->value = "himmel";
  // list->getFirst()->next->next->value = "arsch";
  // cout << list << " original: " << endl << *list << endl;
  // cout << nlist << " new: " << endl << *nlist << endl;
  // delete list;
  // delete nlist;


/* copy of PointerList */
  // MotCle* first = new MotCle("haus", 2);
  // MotCle* second = new MotCle("baum", 54);
  // MotCle* third = new MotCle("affe", 0);
  // PointerList<MotCle>* pList = new PointerList<MotCle>();
  // pList->addElement(first);
  // pList->addElement(second);
  // pList->addElement(third);
  // // cout << pList << " originalList: " << endl << *pList << endl;
  // PointerList<MotCle>* nPList = new PointerList<MotCle>(*pList);
  // cout << pList << " originalList: " << endl << *pList << endl;
  // cout << nPList << " cloneList: " << endl << *nPList << endl;
  // pList->getFirst()->value->motCle = "neuHaus";
  // pList->getFirst()->next->value->motCle = "neuBaum";
  // pList->getFirst()->next->next->value->motCle = "neuAffe";
  // cout << pList << " originalList: " << endl << *pList << endl;
  // cout << nPList << " cloneList: " << endl << *nPList << endl;
  // delete pList;
  // delete nPList;


/* copy of PointerListElement */
  // PointerListElement<MotCle>* oElem = new PointerListElement<MotCle>(oldMot);
  // cout << oElem << " oElem: " << *oElem;
  // PointerListElement<MotCle>* nElem = new PointerListElement<MotCle>(*oElem);
  // cout << nElem << " nElem: " << *nElem;
  // oElem->getValue()->motCle = "baum";
  // cout << oElem << " oElem: " << *oElem;
  // cout << nElem << " nElem: " << *nElem;
  // delete oElem;
  // cout << nElem << " nElem: " << *nElem;


/* copy of List */
  // List<int>* orList = new List<int>();
  // orList->addElement(3);
  // orList->addElement(7);
  // orList->addElement(54);
  // List<int>* newList = new List<int>(*orList);
  // cout << orList << " oldlist: " << *orList << endl;
  // cout << newList << " newlist: " << *newList << endl;
  // orList->getFirst()->value = 233;
  // orList->getFirst()->getNext()->value = 324244;
  // orList->getFirst()->getNext()->getNext()->value = 2342432563;
  // cout << orList << " oldlist: " << *orList << endl;
  // cout << newList << " newlist: " << *newList << endl;


/* copy of ListElement */
  // int zahl = 5;
  // ListElement<int>* origElem = new ListElement<int>(zahl);
  // cout << "original: " << origElem << " " << origElem->getValue() << endl;
  // ListElement<int>* newElem = new ListElement<int>(*origElem);
  // cout << "new: " << newElem << " " << newElem->getValue() << endl;
  // origElem->value = 3;
  // cout << "original: " << origElem << " " << origElem->getValue() << endl;
  // cout << "new: " << newElem << " " << newElem->getValue() << endl;
  // ListElement<int> drittes (*newElem);
  // cout << "third: " << &drittes << " " << drittes.getValue() << endl;
  // newElem->value = 2323;
  // cout << "original: " << origElem << " " << origElem->getValue() << endl;
  // cout << "third: " << &drittes << " " << drittes.getValue() << endl;
  // cout << "new: " << newElem << " " << newElem->getValue() << endl;


/* replace chars in string */
  // string tstring = "hämöü «€¶æÆ ÜÖ QHOÄ SS?ÁÀÚ»ß àé)é==piez)OIPATZI«eæden";
  // cout << tstring << endl << " => " << endl;
  // transform(tstring.begin(), tstring.end(), tstring.begin(), ::tolower);
  // cout << tstring << endl << " => " << endl;
  // for (size_t i = 0; i < NUM_ABNORMS; i++) {
  //   replaceChar(tstring, REPLACERS[i][0], REPLACERS[i][1]);
  // }
  // cout << tstring << endl;


/* multiple stages test */
  // PointerList<Stage>* stageList = new PointerList<Stage>();
  // SortedStringList* newMotsCles = new SortedStringList();
  // newMotsCles->addElement("VW");
  // newMotsCles->addElement("Auto");
  // newMotsCles->addElement("Motor");
  // newMotsCles->addElement("Diesel");
  // Stage* newStage = new Stage("Motorenbau bei VW", "Meier", "Daniel", "2012", newMotsCles);
  // stageList->addElement(newStage);
  // newStage->saveStage();
  // newStage->saveRel();
  //
  // newMotsCles = new SortedStringList();
  // newMotsCles->addElement("BMW");
  // newMotsCles->addElement("Motorrad");
  // newMotsCles->addElement("Kupplung");
  // newMotsCles->addElement("Serienproduktion");
  // newStage = new Stage("Motorradkonstruktion BMW", "Peters", "Lena", "2000", newMotsCles);
  // stageList->addElement(newStage);
  // newStage->saveStage();
  // newStage->saveRel();
  // cout << *stageList;


/* sortedList testcode */
  // SortedStringList* sList = new SortedStringList();
  // sList->addElement("klasse");
  // sList->addElement("hund");
  // sList->addElement("gemuese");
  // cout << *sList;
  // cout << "ist 'hund' in der liste? - " << sList->contains("hund") << endl;
  // cout << "ist 'maus' in der liste? - " << sList->contains("maus") << endl;


/* list and listelement testcode */
  // List<string>* liste = new List<string>;
  // liste->addElement("element 1");
  // liste->addElement("apfel");
  // liste->addElement("kuchen");
  // cout << *liste;
  //
  // List<int>* intList = new List<int>;
  // intList->addElement(5);
  // intList->addElement(32323);
  // intList->addElement(-21);
  // cout << endl << *intList;
}
