#ifndef TOOLS_H
#define TOOLS_H

#include <string>
using namespace std;

char* stToCh(string);
bool lexicoComp(string, string);
void replaceChar(string&, string, string);
int handleOptionInput(int maxInput);
void banSemicolon(string& input, string name, bool logicAllowed = false);

#endif
