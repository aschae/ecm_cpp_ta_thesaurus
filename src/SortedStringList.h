#ifndef SORTEDSTRINGLIST_H
#define SORTEDSTRINGLIST_H
#include "List.h"
#include <string>


class SortedStringList: public List<string>{
public:
  SortedStringList();
  SortedStringList(const SortedStringList&);
  virtual ~SortedStringList();
  void addElement(string); // creates new ListElement and adds it while maintaining lexico order
  string generateString(); // generates string of content: elem_1;elem_2;...;elem_n
  bool contains(string); // checks if element with same string is in list
};

#endif
