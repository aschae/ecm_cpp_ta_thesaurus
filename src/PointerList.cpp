#include "PointerList.h"


template <class T>
PointerList<T>::PointerList() {
  head = NULL;
  length = 0;
}


template <class T>
PointerList<T>::PointerList(const PointerList<T>& opL){
  head = NULL;
  length = 0;
  PointerListElement<T>* origHead = opL.getFirst();
  PointerListElement<T>* curr = origHead;

  if (curr != NULL) {
    do {
      addElement(curr->getValue());
      curr = curr->getNext();
    } while(curr != origHead);
  }
}


template <class T>
PointerList<T>::~PointerList() {
  PointerListElement<T>* origHead = getFirst();
  PointerListElement<T>* curr = origHead;
  PointerListElement<T>* last = NULL;

  if (curr != NULL) {
    do {
      last = curr;
      curr = curr->getNext();
      delete last;
    } while(curr != origHead);
  }
}


// TODO dont insert twice
template <class T>
void PointerList<T>::addElement(T* value) {
  PointerListElement<T>* elem = new PointerListElement<T>(value);
  length++;
  if(head == NULL) {
    head = elem;
    elem->setNext(elem);
    elem->setPrev(elem);
    return;
  }
  head->getPrev()->setNext(elem);
  elem->setPrev(head->getPrev());
  elem->setNext(head);
  head->setPrev(elem);
}


template <class T>
size_t PointerList<T>::getLength() {
  return length;
}


template <class T>
PointerListElement<T>* PointerList<T>::getFirst() const {
  return head;
}


template <class T>
ostream& operator<<(ostream& os, const PointerList<T>& perso) {
  PointerListElement<T>* head = perso.getFirst();
  if (head != NULL) {
    os << *head;
    PointerListElement<T>* next = head->getNext();
    while(next != head) {
      os << *next;
      next = next->getNext();
    }
  }
  return os;
}
