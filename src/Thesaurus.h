#ifndef THESAURUS_H
#define THESAURUS_H

#include "SortedStringList.h"
#include "PointerList.h"
#include "tools.h"


// contains all relevant functions and data for the thesaurus
// does not allow duplication of values
class Thesaurus : public SortedStringList {
private:
  PointerList<SortedStringList>* equiv; // each SSL represents an equivalence class
  size_t printEquiv(); // prints equivalent classes
public:
  Thesaurus();
  Thesaurus(const Thesaurus&);
  virtual ~Thesaurus();
  PointerList<SortedStringList>* getEquiv(); // returns list of equiv classes
  SortedStringList* getClass(string); // returns equivalence class of string if existent, else NULL
  void addElement(string element, bool silent = false); // checks if mc already exisits, if not adds to list at the right position in lexikographic order
  void addList(const SortedStringList* motsCles); // adds elements of list to thesaurus while maintaining previous data
  void addClasses(PointerList<SortedStringList>* eqClasses); // overrides eqclasses
  friend ostream& operator<<(ostream& os, const Thesaurus& perso);
};

#endif
