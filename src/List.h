#ifndef LIST_H
#define LIST_H

#include "ListElement.h"
#include <string>
using namespace std;


// List for values (not reference)
template <class T>
class List {
protected:
  ListElement<T>* head;
  size_t length;
public:
  List();
  List(const List&);
  virtual ~List ();
  void addElement(T element); //creates new ListElement with value and appends on list
  ListElement<T>* getFirst() const; //get first listElement of list
  size_t getLength(); // get number of elements in list
  template <class U>
  friend ostream& operator<<(ostream& os, const List<U>& perso); //
};
#include "List.cpp"
#endif
