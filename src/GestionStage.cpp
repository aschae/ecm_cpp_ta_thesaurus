// compile with "g++ -Wall -std=c++11 GestionStage.cpp SortedStringList.cpp tools.cpp Stage.cpp Thesaurus.cpp -o GestionStage"
#include <iostream>
#include <fstream>
#include <string>
#include <regex>

#include "Stage.h"
#include "SortedStringList.h"
#include "PointerList.h"
#include "Thesaurus.h"
#include "tools.h"

// TODO check getters for corruption
using namespace std;
enum optionLevel {MAIN_MENUE, STAGE};


// save relations between stage title and motcles in file
bool writeAllRelations(PointerList<Stage>* stageList) {
  ofstream f;
  f.open("../data/relations.txt", ios::out | ios::trunc);
  if (f.is_open())
  {
    f.close();
  }
  else {
    cout << "Unable to open file" << endl;
    return false;
  }
  PointerListElement<Stage>* firstStage = stageList->getFirst();
  PointerListElement<Stage>* currStage = firstStage;

  if (firstStage == NULL) {
    return true;
  }
  currStage->getValue()->saveRel();
  currStage = currStage->getNext();
  while(currStage != firstStage) {
    currStage->getValue()->saveRel();
    currStage = currStage->getNext();
  }
  return true;
}


// write stages in file
bool writeAllStages(PointerList<Stage>* stageList) {
  ofstream f;
  f.open("../data/stages.txt", ios::out | ios::trunc);
  if (f.is_open())
  {
    f.close();
  }
  else {
    cout << "Unable to open file" << endl;
    return false;
  }
  PointerListElement<Stage>* firstStage = stageList->getFirst();
  PointerListElement<Stage>* currStage = firstStage;

  if (firstStage == NULL) {
    return true;
  }
  currStage->getValue()->saveStage();
  currStage = currStage->getNext();
  while(currStage != firstStage) {
    currStage->getValue()->saveStage();
    currStage = currStage->getNext();
  }
  return true;
}


// write mc of thesaurus in one file, equivalences in another
bool writeThesaurus(Thesaurus* thes) {
  ofstream f;
  f.open("../data/thesaurus.txt", ios::out | ios::trunc);
  if (f.is_open())
  {
    f << *thes;
    f.close();
    // return true;
  }
  else {
    cout << "Unable to open file" << endl;
    return false;
  }

  f.open("../data/equivalences.txt", ios::out | ios::trunc);
  if (f.is_open())
  {
    PointerList<SortedStringList>* equiv = thes->getEquiv();
    PointerListElement<SortedStringList>* firstList = equiv->getFirst();
    PointerListElement<SortedStringList>* currentList = firstList;
    if (currentList != NULL) {
      do {
        f << currentList->getValue()->generateString() << endl;
        currentList = currentList->getNext();
      } while(currentList != firstList);
    }
    f.close();
    return true;
  }
  else {
    cout << "Unable to open file" << endl;
    return false;
  }
}


// read relations from file and build up objects
bool readRelations(PointerList<Stage>* stageList) {
  ifstream f;
  string line;
  f.open("../data/relations.txt", ios::in);
  if (f.is_open())
  {
    size_t startPos;
    size_t semic;
    string title;
    SortedStringList* newList;
    while(getline(f, line)) {
      newList = new SortedStringList();
      startPos = 0;
      semic = line.find_first_of(";", startPos);
      title = line.substr(startPos, semic-startPos);
      startPos = semic+1;
      while((semic = line.find_first_of(";", startPos)) != (string::npos)) {
        newList->addElement(line.substr(startPos, semic-startPos));
        startPos = semic+1;
      }
      newList->addElement(line.substr(startPos, line.size()-startPos));

      PointerListElement<Stage>* first = stageList->getFirst();
      PointerListElement<Stage>* curr = first;
      if (first == NULL) {
        delete newList;
        return false;
      }

      if (curr->getValue()->getTitre().compare(title) == 0) {
        curr->getValue()->setMotsCles(newList);
      }
      curr = curr->getNext();
      while(curr != first) {
        if (curr->getValue()->getTitre().compare(title) == 0) {
          curr->getValue()->setMotsCles(newList);
        }
        curr = curr->getNext();
    }
    delete newList;
  }
    f.close();
  }
  else {
    cout << "Unable to open file" << endl;
    return false;
  }
  return true;
}


// read stages from file and build up objects
bool readStages(PointerList<Stage>* stageList) {
  ifstream f;
  string line;
  f.open("../data/stages.txt", ios::in);
  if (f.is_open())
  {
    size_t startPos;
    size_t semic;
    string newValues[4];
    Stage* newStage;
    int i;
    while(getline(f, line)) {
      startPos = 0;
      i = 0;
      while((semic = line.find_first_of(";", startPos)) != (string::npos)) {
        newValues[i] = line.substr(startPos, semic-startPos);
        startPos = semic+1;
        i++;
      }
      newValues[3] = line.substr(startPos, line.size()-startPos);
      newStage = new Stage(newValues[0],newValues[1],newValues[2],newValues[3]);
      stageList->addElement(newStage);
      delete newStage;
    }
    f.close();
  }
  else {
    cout << "Unable to open file" << endl;
    return false;
  }
  return true;
}


// read thesaurus from file and build up objects
bool readThesaurus(Thesaurus* thes) {
  ifstream f;
  string line;
  f.open("../data/thesaurus.txt", ios::in);
  if (f.is_open())
  {
    while(getline(f, line)) {
      thes->addElement(line, true);
    }
    f.close();
  }
  else {
    cout << "Unable to open file" << endl;
    return false;
  }


  f.open("../data/equivalences.txt", ios::in);
  if (f.is_open())
  {
    size_t startPos;
    size_t semic;
    PointerList<SortedStringList>* equiList = new PointerList<SortedStringList>();
    SortedStringList* eClass;
    while(getline(f, line)) {
      eClass = new SortedStringList();
      startPos = 0;
      while((semic = line.find_first_of(";", startPos)) != (string::npos)) {
        eClass->addElement(line.substr(startPos, semic-startPos));
        startPos = semic+1;
      }
      eClass->addElement(line.substr(startPos, line.size()-startPos));
      equiList->addElement(eClass);
      delete eClass;
    }
    thes->addClasses(equiList);
    delete equiList;
    f.close();
  } else {
    return false;
  }


  // cout << "restorofed Thesaurus:" << endl << *thes << endl;
  return true;
}


// filters list for equiclass for given mot-cle
// returns a new list
PointerList<Stage>* searchEqui(string mc, PointerList<Stage>* stageList, Thesaurus* thes) {
  SortedStringList* classe = thes->getClass(mc);

  PointerList<Stage>* results = new PointerList<Stage>();
  if (classe == NULL) {
    return results;
  }
  PointerListElement<Stage>* first = stageList->getFirst();
  PointerListElement<Stage>* current = first;
  SortedStringList* currMotsCles;
  ListElement<string>* firstString;
  ListElement<string>* currString;

  do {
    currMotsCles = current->getValue()->getMotsCles();
    firstString = classe->getFirst();
    currString = firstString;
    do {
      if (currMotsCles->contains(currString->getValue())) {
        results->addElement(current->getValue());
        break;
      }
      currString = currString->getNext();
    } while(firstString != currString);
    current = current->getNext();
  }  while (current != first);
  return results;
}


// search stages by motcle
size_t searchTitles(string input, PointerList<Stage>* stageList, Thesaurus* thes, bool numberMode = false) {
  PointerList<Stage>* results = searchEqui(input, stageList, thes);
  if (!numberMode) {
    cout << *results;
  }
  size_t hits = results->getLength();
  delete results;
  return hits;
}


// test logical expression with stages
PointerList<Stage>* testLogicalExpr(PointerList<Stage>* stageList, Thesaurus* thes, PointerList<SortedStringList>* logical) {
  PointerListElement<SortedStringList>* firstList = logical->getFirst();
  PointerListElement<SortedStringList>* currentList = firstList;
  ListElement<string>* firstElem;
  ListElement<string>* currentElem;
  PointerList<Stage>* prevResults = new PointerList<Stage>();
  PointerList<Stage>* currResults;
  PointerList<Stage>* lastResults;
  PointerListElement<Stage>* firstPLE;
  PointerListElement<Stage>* currPLE;

  do {
    currResults = new PointerList<Stage>(*stageList);
    firstElem = currentList->getValue()->getFirst();
    currentElem = firstElem;
    do {
      lastResults = currResults;
      currResults = searchEqui(currentElem->getValue(), currResults, thes);
      delete lastResults;
      currentElem = currentElem->getNext();
    } while(firstElem != currentElem);

    firstPLE = currResults->getFirst();
    currPLE = firstPLE;
    if (currPLE != NULL) {
      do {
        prevResults->addElement(currPLE->getValue());
        currPLE = currPLE->getNext();
      } while(currPLE != firstPLE);
    }
    delete currResults;
    currentList = currentList->getNext();
  } while(firstList != currentList);

  return prevResults;
}


// search stages by logical expression
void searchLogical(PointerList<Stage>* stageList, Thesaurus* thes) {
  string input;
  SortedStringList* hits = new SortedStringList();
  string hit;

  // begin get logical expression
  banSemicolon(input, "Enter logical expression (use + for OR and . for AND)", true);
  if(stageList->getFirst() == NULL) {
    cout << endl << "Résultats:" << endl;
    return;
  }
  PointerList<SortedStringList>* logicList = new PointerList<SortedStringList>;
  SortedStringList* currList = new SortedStringList();
  string currMot;
  size_t head;
  size_t tail = 0;
  for (head = 0; head < input.size(); head++) {
    if (input[head] == '+' || (input[head] == '.')) {
      currMot = input.substr(tail, head-tail);
      currList->addElement(currMot);
      tail = head+1;
      if (input[head] == '+') {
        logicList->addElement(currList);
        delete currList;
        currList = new SortedStringList();
      }
    }
  }
  currMot = input.substr(tail, head-tail);

  currList->addElement(currMot);
  logicList->addElement(currList);
  delete currList;
  // end getlocigal expresssion
  PointerList<Stage>* results = testLogicalExpr(stageList, thes, logicList);

  PointerListElement<Stage>* origHead = results->getFirst();
  PointerListElement<Stage>* curr = origHead;
  Stage* hitStage;

  if (curr != NULL) {
    do {
      hitStage = curr->getValue();
      hit = hitStage->getNom() + " " + hitStage->getPrenom() + ", Promo " + hitStage->getPromo();
      if(!hits->contains(hit)) {
        hits->addElement(hit);
      }
      curr = curr->getNext();
    } while(curr != origHead);
  }
  cout << endl << "Résultats:" << endl;
  cout << *hits;
  delete hits;
  delete logicList;
  delete results;
}


// list all motcles and the number of their appearance
void numberOfStages(PointerList<Stage>* stageList, Thesaurus* thesaur) {
  ListElement<string>* first = thesaur->getFirst();
  ListElement<string>* curr = first;
  string currMot;
  int currNumber;
  if (first == NULL) {
    return;
  }
  do {
    currMot = curr->getValue();
    currNumber = searchTitles(currMot, stageList, thesaur, true);
    cout << currMot << ": " << currNumber << endl;
    curr = curr->getNext();
  } while(first != curr);
}


int main(int argc, char const *argv[]) {
  bool save = true;
  PointerList<Stage>* stageList = new PointerList<Stage>;
  Thesaurus* thesaur = new Thesaurus();
  readThesaurus(thesaur);
  readStages(stageList);
  readRelations(stageList);
  string input;
  optionLevel option = MAIN_MENUE;
  int choice;
  string choiceStr;
  bool loop = true;
  cout << endl << "~~~ Bienvenu chez << GestionStage >> ~~~" << endl;
  while(loop) {
    switch (option) {
      case MAIN_MENUE:
        cout << "MENU PRINCIPAL" << endl << endl;
        cout << "1 - Ajouter stage" << endl;
        cout << "2 - Recherche des sujets comportant un mot-clé donné" << endl;
        cout << "3 - Recherche des élèves et de leur promotion au facon logique" << endl;
        cout << "4 - Recherche, pour chaque mot-clé existant dans le thésaurus, du" <<
        " nombre de sujets" << endl;
        cout << "5 - Afficher stages" << endl;
        if(save) {
          cout << "6 - [ ] Supprimer tous les données pendant fermeture" << endl;
        } else {
          cout << "6 - [x] Supprimer tous les données pendant fermeture" << endl;
        }
        cout << "------" << endl;
        cout << "7 - terminer logiciel" << endl;
        choice = handleOptionInput(7);
        cout << "---------------------------------------------------" << endl;
        switch (choice) {
          case 0:
            break;
          case 1:
            option = STAGE;
            break;
          case 2:
            // option = RECH_MC;
            cout << "RECHERCHE DES SUJETS PAR MOT-CLE" << endl << endl;
            banSemicolon(input, "Mot-Cle");
            cout << endl << "Résultats:" << endl;
            searchTitles(input, stageList, thesaur);
            cout << "---------------------------------------------------" << endl;
            break;
          case 3:
            cout << "RECHERCHE DES ELEVES ET LEUR PROMO PAR EXPRESSION LOGIQUE" << endl << endl;
            searchLogical(stageList, thesaur);
            cout << "---------------------------------------------------" << endl;
            break;
          case 4:
            // option = RECH_NUM;
            cout << "NOMBRE DES SUJETS PAR MOT-CLE" << endl << endl;
            numberOfStages(stageList, thesaur);
            cout << "---------------------------------------------------" << endl;
            break;
          case 5:
            cout << "LISTE DES STAGES" << endl << endl;
            cout << *stageList;
            cout << "---------------------------------------------------" << endl;
            break;
          case 6:
            save = !save;
            break;
          case 7:
            loop = false;
            break;
        }
        break;
      case STAGE:
      {
        cout << "AJOUTER STAGE" << endl << endl;
        string titre;
        string nom;
        string prenom;
        string promo;
        SortedStringList* motsCles = new SortedStringList();
        Stage* newStage;

        string tempMot;

        banSemicolon(titre, "Titre");
        banSemicolon(nom, "Nom");
        banSemicolon(prenom, "Prenom");
        banSemicolon(promo, "Promo");
        banSemicolon(tempMot, "Mot-Cle (type 'done' to quit)");

        while(tempMot.compare("done") != 0)
        {
          motsCles->addElement(tempMot);
          banSemicolon(tempMot, "Mot-Cle (type 'done' to quit)");
        }
        string save;
        bool saveLoop = true;
        cout << "---------------------------------------------------" << endl;
        cout << "ENREGISTRER NOUVEAU STAGE" << endl << endl;
        cout << "Titre = " << titre << endl;
        cout << "Nom = " << nom << endl;
        cout << "Prenom = " << prenom << endl;
        cout << "Promo = " << promo << endl;
        cout << "Mots-Cles = " << motsCles->generateString() << endl;
        while(saveLoop) {
          cout << endl << "(oui) ou (non)? ";
          getline(cin, save);
          if (save.compare("oui") == 0) {
            newStage = new Stage(titre, nom, prenom, promo, motsCles);
            stageList->addElement(newStage);
            thesaur->addList(motsCles);
            delete newStage;
            delete motsCles;
            cout << "Stage enregistré" << endl;
            saveLoop = false;
          }
          else if (save.compare("non") == 0) {
            delete motsCles;
            cout << endl << "Stage pas enregistré" << endl;
            saveLoop = false;
          }
          else {
            cout << endl << "Wrong input! Try again!" << endl;
          }
        }
        cout << "---------------------------------------------------" << endl;
        option = MAIN_MENUE;
        break;
      }
    }
  }
  cout << "programm terinating..." << endl;
  if (save) {
    writeAllRelations(stageList);
    writeAllStages(stageList);
    writeThesaurus(thesaur);
  } else {
    ofstream g;
    g.open("../data/relations.txt", ios::out | ios::trunc);
    g.close();
    g.open("../data/thesaurus.txt", ios::out | ios::trunc);
    g.close();
    g.open("../data/stages.txt", ios::out | ios::trunc);
    g.close();
    g.open("../data/equivalences.txt", ios::out | ios::trunc);
    g.close();
  }
  delete stageList;
  delete thesaur;
  cout << "programm terminated" << endl;

  return 0;
}
