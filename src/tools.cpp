#include <string>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

const size_t NUM_ABNORMS = 35;
const string REPLACERS[NUM_ABNORMS][2] = {{"ä", "ae"}, {"Ä", "ae"}, {"à", "a"}, {"À", "a"},
{"æ", "ae"}, {"Æ", "ae"}, {"ç", "c"}, {"Ç", "c"}, {"é", "e"}, {"É", "e"},
{"è", "e"}, {"È", "e"}, {"Ê", "e"}, {"ê", "e"}, {"Ë", "e"}, {"ë", "e"},
{"Î", "i"}, {"î", "i"}, {"Ï", "i"}, {"ï", "i"}, {"ö", "oe"}, {"Ö", "oe"},
{"Ô", "o"}, {"ô", "o"}, {"Œ", "oe"}, {"œ", "oe"}, {"ß", "ss"}, {"ü", "ue"},
{"Ü", "ue"}, {"Ù", "u"}, {"ù", "u"}, {"Û", "u"}, {"û", "u"}, {"Ÿ", "y"}, {"ÿ", "y"}};

// ch hast to be same size
char* stToCh(string str) {
  size_t newValSize = str.size();
  char* newC = new char[newValSize+1];
  str.copy(newC, newValSize);
  newC[newValSize] = '\0';
  return newC;
}


void replaceChar(string& str, string oldVal, string newVal) {
  size_t pos;
  while((pos = str.find(oldVal)) != string::npos) {
    str.replace(pos, oldVal.size(), newVal);
  }
}


bool lexicoComp(string first, string second) {
  // if(first == "")
  transform(first.begin(), first.end(), first.begin(), ::tolower);
  transform(second.begin(), second.end(), second.begin(), ::tolower);
  for (size_t i = 0; i < NUM_ABNORMS; i++) {
    replaceChar(first, REPLACERS[i][0], REPLACERS[i][1]);
    replaceChar(second, REPLACERS[i][0], REPLACERS[i][1]);
  }
  char* newVal = stToCh(second);
  size_t newValSize = second.size();
  char* currVal = stToCh(first);
  size_t currValSize = first.size();
  bool result = lexicographical_compare(currVal, currVal+(currValSize), newVal, newVal+(newValSize));
  delete[] newVal;
  delete[] currVal;
  return result;
}


// converts input to string, returns 0 if input not adequate
int handleOptionInput(int maxInput) {
  string input;
  cout << endl << "Choose option: ";
  getline(cin, input);
  // cin >> input;
  int choiceInt;
  try {
    choiceInt = stoi(input);
    if (choiceInt < 1 || choiceInt > maxInput) {
      cout << "---------------------------------------------------" << endl;
      cout << "ERROR! ENTER NATURAL NUMBER FROM 1 TILL " << maxInput << "." << endl;
      return 0;
    } else {
      return choiceInt;
    }
  }
  catch (const std::invalid_argument) {
    cout << "---------------------------------------------------" << endl;
    cout << "ERROR! ENTER NATURAL NUMBER FROM 1 TILL " << maxInput << "." << endl;
    return 0;
  }
}


void banSemicolon(string& input, string name, bool logicAllowed = false) {
  cout << name << ": ";
  string prohibited;
  if (logicAllowed) {
    prohibited = ";";
  } else {
    prohibited = ".+;";
  }
  getline(cin, input);
  while (input.find_first_of(prohibited) != string::npos || input=="") {
    cout << "'" << prohibited << "' and empty input not allowed, please try again" << endl
    << name << ": ";
    getline(cin, input);
  }
}
