#ifndef LISTELEMENT_H
#define LISTELEMENT_H
#include <string>

using namespace std;


// listElement for List (value, not reference)
template <class T>
class ListElement {
private:
  T value; // or string motCle
  ListElement<T>* next;
  ListElement<T>* prev;
public:
  ListElement(T nValue);
  ListElement(const ListElement<T>&);
  virtual ~ListElement();
  T getValue() const;
  ListElement<T>* getNext() const; // get next listelement
  ListElement<T>* getPrev() const; // get previous listelement
  void setNext(ListElement<T>* nNext); // set next listelement
  void setPrev(ListElement<T>* nPrev); // set prev listelement
  template <class U>
  friend ostream& operator<<(ostream& os, const ListElement<U>& perso);
};
#include "ListElement.cpp"
#endif
