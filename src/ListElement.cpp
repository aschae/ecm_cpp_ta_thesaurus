#include "ListElement.h"
#include <iostream>


template <class T>
ListElement<T>::ListElement(T nValue) {
  value = nValue;
  next = NULL;
  prev = NULL;
}


template <class T>
ListElement<T>::ListElement(const ListElement<T>& le){
  value = le.getValue();
  next = le.getNext();
  prev = le.getPrev();
}


template <class T>
ListElement<T>::~ListElement() {
}


template <class T>
T ListElement<T>::getValue() const {
  return value;
}


template <class T>
ListElement<T>* ListElement<T>::getNext() const {
  return next;
}


template <class T>
ListElement<T>* ListElement<T>::getPrev() const {
  return prev;
}


template <class T>
void ListElement<T>::setNext(ListElement<T>* nNext) {
  next = nNext;
}


template <class T>
void ListElement<T>::setPrev(ListElement<T>* nPrev) {
  prev = nPrev;
}


template <class T>
ostream& operator<<(ostream& os, const ListElement<T>& perso) {
  os << perso.getValue() << endl;
  return os;
}
