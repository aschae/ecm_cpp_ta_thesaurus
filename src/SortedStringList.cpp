#include "SortedStringList.h"
#include "tools.h"
#include <algorithm>
#include <cstdio>
#include <iostream>


SortedStringList::SortedStringList() : List<string>() {
  return;
}


SortedStringList::SortedStringList(const SortedStringList& sourceList) {
  head = NULL;
  length = 0;
  ListElement<string>* origHead = sourceList.getFirst();
  ListElement<string>* curr = origHead;

  if (curr != NULL) {
    do {
      addElement(curr->getValue());
      curr = curr->getNext();
    } while(curr != origHead);
  }
}


SortedStringList::~SortedStringList() {
}


void SortedStringList::addElement(string value){
  length++;
  ListElement<string>* elem = new ListElement<string>(value);
  if(head == NULL) {
    head = elem;
    elem->setNext(elem);
    elem->setPrev(elem);
    return;
  }
  else {
    ListElement<string>* current = head;
    if (!lexicoComp(current->getValue(), elem->getValue())) {
      elem->setPrev(current->getPrev());
      elem->setNext(current);
      elem->getPrev()->setNext(elem);
      elem->getNext()->setPrev(elem);
      head = elem;
      return;
    }
    current = current->getNext();
    while(current != head && lexicoComp(current->getValue(), elem->getValue())) {
      current = current->getNext();
    }
    elem->setPrev(current->getPrev());
    elem->setNext(current);
    elem->getPrev()->setNext(elem);
    elem->getNext()->setPrev(elem);
    return;
  }
}


string SortedStringList::generateString(){
  string content = "";
  if (head == NULL) {
    return content;
  }
  content += head->getValue();
  ListElement<string>* current = head->getNext();
  while(current != head) {
    content = content + ";" + current->getValue();
    current = current->getNext();
  }
  return content;
}


bool SortedStringList::contains(string elem){
  if (head == NULL) {
    return false;
  }
  string currVal = head->getValue();
  if (elem.compare(currVal) == 0) {
    return true;
  }

  ListElement<string>* current = head->getNext();
  while(current != head && !lexicoComp(elem, current->getValue())) {
    currVal = current->getValue();

    if (elem.compare(currVal) == 0) {
      return true;
    }
    current = current->getNext();
  }
  return false;
}
