#include "Stage.h"
#include <iostream>
#include <fstream>
using namespace std;


Stage::Stage(string titre, string nom, string prenom, string promo, SortedStringList* motsCles)
{
  this->titre = titre;
  this->nom = nom;
  this->prenom = prenom;
  this->promo = promo;
  if (motsCles == NULL) {
    this->motsCles = NULL;
  } else {
    this->motsCles = new SortedStringList(*motsCles);
  }
}


Stage::Stage(const Stage& old) {
  titre = old.getTitre();
  nom = old.getNom();
  prenom = old.getPrenom();
  promo = old.getPromo();
  if (old.getMotsCles() == NULL) {
    motsCles = NULL;
  } else {
    motsCles = new SortedStringList(*old.getMotsCles());
  }
}


Stage::~Stage() {
  if(motsCles != NULL) {
    delete motsCles;
  }
}


string Stage::getTitre() const {
  return titre;
}


string Stage::getNom() const {
  return nom;
}


string Stage::getPrenom() const {
  return prenom;
}


string Stage::getPromo() const {
  return promo;
}


SortedStringList* Stage::getMotsCles() const {
  return motsCles;
}


void Stage::setMotsCles(SortedStringList* newList) {
  if (getMotsCles() == NULL) {
    motsCles = new SortedStringList(*newList);
  }
}


void Stage::saveRel()
{
  ofstream f;
    f.open("../data/relations.txt", ios::out | ios::app);
    if (f.is_open())
    {
      f << titre << ";" << motsCles->generateString() << endl;
      f.close();
    }
    else cout << "Unable to open file" << endl;
}


void Stage::saveStage()
{
  ofstream f;
    f.open("../data/stages.txt", ios::out | ios::app);
    if (f.is_open())
    {
      f << titre << ";" << nom << ";" << prenom << ";" << promo << endl;
      f.close();
    }
    else cout << "Unable to open file" << endl;
}


ostream& operator<<(ostream& os, const Stage& perso) {
  os << perso.getTitre() << ";" << perso.getNom() << ";" << perso.getPrenom() << ";" << perso.getPromo() << ";" << (perso.getMotsCles() == NULL ? "" : perso.getMotsCles()->generateString());
  return os;
}
