#ifndef POINTERLISTELEMENT_H
#define POINTERLISTELEMENT_H
#include <string>
// #include "ListElement.h"
using namespace std;


// ListElement for PointerList
template <class T>
class PointerListElement {
private:
  T* value; // or string motCle
  PointerListElement<T>* next;
  PointerListElement<T>* prev;
public:
  PointerListElement(T* nValue);
  PointerListElement(const PointerListElement&);
  virtual ~PointerListElement();
  T* getValue() const;
  PointerListElement<T>* getNext() const;
  PointerListElement<T>* getPrev() const;
  void setNext(PointerListElement<T>* nNext);
  void setPrev(PointerListElement<T>* nPrev);
  template <class U>
  friend ostream& operator<<(ostream& os, const PointerListElement<U>& perso);
};
#include "PointerListElement.cpp"
#endif
