#ifndef POINTERLIST_H
#define POINTERLIST_H

#include "PointerListElement.h"
#include <string>
using namespace std;


// list for references/pointers
template <class T>
class PointerList {
protected:
  PointerListElement<T>* head; //type
  size_t length;
public:
  PointerList(); // type
  PointerList(const PointerList&);
  virtual ~PointerList ();
  void addElement(T* element); // creates new PointeListElement and appends it
  size_t getLength(); // get number of elements in list
  PointerListElement<T>* getFirst() const; // get first element of list
  template <class U>
  friend ostream& operator<<(ostream& os, const PointerList<U>& perso);
};
#include "PointerList.cpp"
#endif
