#include "PointerListElement.h"
#include <iostream>

template <class T>
PointerListElement<T>::PointerListElement(T* nValue) {
  value = new T(*nValue);
  next = NULL;
  prev = NULL;
}


template <class T>
PointerListElement<T>::PointerListElement(const PointerListElement& pLE) {
  value = new T(*pLE.getValue());
  next = pLE.getNext();
  prev = pLE.getPrev();
}


template <class T>
PointerListElement<T>::~PointerListElement() {
  delete value;
  return;
}


template <class T>
T* PointerListElement<T>::getValue() const {
  return value;
}


template <class T>
PointerListElement<T>* PointerListElement<T>::getNext() const {
  return next;
}


template <class T>
PointerListElement<T>* PointerListElement<T>::getPrev() const {
  return prev;
}


template <class T>
void PointerListElement<T>::setNext(PointerListElement<T>* nNext) {
  next = nNext;
}


template <class T>
void PointerListElement<T>::setPrev(PointerListElement<T>* nPrev) {
  prev = nPrev;
}


template <class T>
ostream& operator<<(ostream& os, const PointerListElement<T>& perso) {
  os << *perso.getValue() << endl;
  return os;
}
