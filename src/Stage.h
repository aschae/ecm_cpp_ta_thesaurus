#ifndef STAGE_H
#define STAGE_H

#include <string>
#include "SortedStringList.h"
using namespace std;


// this object represents a STage
class Stage {
private:
  string titre; // titre de stage
  string nom; // nom d'eleve
  string prenom; // prenom d'eleve
  string promo; // promo d'eleve
  SortedStringList* motsCles; // liste des mots-cles
public:
  Stage(string titre, string nom, string prenom, string promo, SortedStringList* motsCles = NULL);
  Stage(const Stage&);
  virtual ~Stage ();
  string getTitre() const;
  string getNom() const;
  string getPrenom() const;
  string getPromo() const;
  SortedStringList* getMotsCles() const;
  void setMotsCles(SortedStringList*);
  void saveRel(); // appends "title; mc_1; mc_2; ...; mc_n" to "data/relations.txt"
  void saveStage(); // appends "titre; nom; prenom; promo" to "data/stages.txt"
  friend ostream& operator<<(ostream& os, const Stage& perso);
};

#endif
