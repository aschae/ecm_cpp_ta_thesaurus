#include "List.h"


template <class T>
List<T>::List() {
  head = NULL;
  length = 0;
}


template <class T>
List<T>::List(const List<T>& sourceList) {
  head = NULL;
  length = 0;
  ListElement<T>* origHead = sourceList.getFirst();
  ListElement<T>* curr = origHead;

  if (curr != NULL) {
    do {
      addElement(curr->getValue());
      curr = curr->getNext();
    } while(curr != origHead);
  }
}


template <class T>
List<T>::~List() {
  ListElement<T>* origHead = getFirst();
  ListElement<T>* curr = origHead;
  ListElement<T>* last = NULL;

  if (curr != NULL) {
    do {
      last = curr;
      curr = curr->getNext();
      delete last;
    } while(curr != origHead);
  }
}


// TODO dont insert twice
template <class T>
void List<T>::addElement(T value) {
  ListElement<T>* elem = new ListElement<T>(value);
  length++;
  if(head == NULL) {
    head = elem;
    elem->setNext(elem);
    elem->setPrev(elem);
    return;
  }
  head->getPrev()->setNext(elem);
  elem->setPrev(head->getPrev());
  elem->setNext(head);
  head->setPrev(elem);
}


template <class T>
ListElement<T>* List<T>::getFirst() const {
  return head;
}


template <class T>
size_t List<T>::getLength() {
  return length;
}


template <class T>
ostream& operator<<(ostream& os, const List<T>& perso) {
  ListElement<T>* head = perso.getFirst();
  if (head != NULL) {
    os << *head;
    ListElement<T>* next = head->getNext();
    while(next != head) {
      os << *next;
      next = next->getNext();
    }
  }
  return os;
}
